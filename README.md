# Primary

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Consideraciones

Para este proyecto se generaron apis de consumo de datos utilizando json-server por un lado y node, express con fs por el otro.

### Para los datos de los usuarios y direcciones se utilizó json-server:

`https://github.com/typicode/json-server`

Primero 

se instala json-server

```
$ npm install -g json-server
```
Segundo

creamos un archivo json, por ejemplo "db.json" con los datos que queremos exponer
con el siguiente formato:

```
{
  "endpoint": [
      { datos...},
      { datos...},
  ],  
  "endpoint2": [
      { datos...},
      { datos...},
  ]
}
```

Tercero 

se ejecuta el comando

```
$ json-server --watch db.json
```

Luego las api ya estan disponibles.

Los siguientes HTTP endpoints son creados automaticamente por JSON server:

* GET    `http://localhost:3000/endpoint`
* GET    `http://localhost:3000/endpoint/{id}`
* POST   `http://localhost:3000/endpoint`
* PUT    `http://localhost:3000/endpoint/{id}`
* PATCH  `http://localhost:3000/endpoint/{id}`
* DELETE `http://localhost:3000/endpoint/{id}`

Es posible extender las URL con más parámetros. Por ej. puede aplicar el filtrado utilizando parámetros de URL como puede ver en los ejemplos siguientes:

* `http://localhost:3000/listaDireccionC?personaId=1`
* `http://localhost:3000/listaDireccionC?q=Buenos`

### Para los datos de los logins se utilizó node, express con fs :

```
$ npm install -g express-generator
$ npm install express --save
```
Se creo un archivo con el programa llamado server.js
Luego se inicia
```
$ node server.js
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
