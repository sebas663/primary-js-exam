const express = require("express");
const fs = require("fs");

const app = express();
const port = 3001;

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

  // Pass to next layer of middleware
  next();
});

app.get("/v1/LoginInfo/UserRecentsLogins", (req, res) =>
  res.send(getUserRecentsLogins())
);
app.get("/v1/LoginInfo/UsersWithMoreLogins", (req, res) =>
  res.send(getUsersWithMoreLogins())
);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

let rawdata = fs.readFileSync("login.json");

const getUserRecentsLogins = () => {
  let loginInfo = JSON.parse(rawdata);
  loginInfo.sort(compareValues("loginDate", "desc", "date"));
  const userRecentsLogins = loginInfo.slice(0, 3);
  return userRecentsLogins;
};

const getUsersWithMoreLogins = () => {
  let loginInfo = JSON.parse(rawdata);
  var result = [];
  loginInfo.reduce((res, value) => {
    if (!res[value.id]) {
      res[value.id] = { id: value.id, qty: 0 };
      result.push(res[value.id]);
    }
    res[value.id].qty += 1;
    return res;
  }, {});
  result.sort(compareValues("qty", "desc"));
  let userMaxLogins = result.slice(0, 3);
  let usersWithMoreLogins = [];
  for (const pos in userMaxLogins) {
    const filtered = loginInfo.filter(a => a.id === userMaxLogins[pos].id);
    usersWithMoreLogins = [...usersWithMoreLogins, ...filtered ];
  }
  return usersWithMoreLogins;
};

const compareValues = (key, order = "asc", type) => {
  return (a, b) => {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      return 0;
    }
    const varA = getComparisonVar(a[key], type);
    const varB = getComparisonVar(b[key], type);

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order == "desc" ? comparison * -1 : comparison;
  };
};

getComparisonVar = (value, type) => {
  let comparisonVar = value;
  if (type === "date") {
    comparisonVar = new Date(value);
  }
  return comparisonVar;
};