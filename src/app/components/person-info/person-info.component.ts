import { Component, OnInit } from "@angular/core";
import { User } from "../../models/user";
import { UserDetail } from "../../models/user-detail";
import { UserService } from "../../services/user-service";

@Component({
  selector: "app-person-info",
  templateUrl: "./person-info.component.html",
  styleUrls: ["./person-info.component.css"]
})
export class PersonInfoComponent implements OnInit {
  users: User[] = [];
  userDetails: UserDetail[] = [];

  constructor(private userService: UserService) {
    const usersPromise =  this.userService.getUsers();
    usersPromise.then(result => this.users = result);

    const promise =  this.userService.obtenerListadoPersonas("");
    promise.then(result => this.userDetails = result);
  }

  ngOnInit() {}
}
