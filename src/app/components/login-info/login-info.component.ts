import { Component, OnInit } from "@angular/core";
import { LoginInfo } from "../../models/login-info";
import { UserLoginInfo } from "../../models/user-login-info";
import { LoginService } from "../../services/login.service";

@Component({
  selector: "app-login-info",
  templateUrl: "./login-info.component.html",
  styleUrls: ["./login-info.component.css"]
})
export class LoginInfoComponent implements OnInit {
  
  maxLoginInfo: UserLoginInfo[] = [];
  recentLoginInfo: LoginInfo[] = [];

  constructor(private loginService: LoginService) {
    const plm = this.loginService.getUsersWithMoreLogins();
    plm.then(result => this.maxLoginInfo = result);
    const prl = this.loginService.getUserRecentsLogins();
    prl.then(result => this.recentLoginInfo = result);
  }

  ngOnInit() {}
}
