import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import { Address } from "../models/address";
import { PostalCode } from "../models/postal-code";
import { UserDetail } from "../models/user-detail";
import { AddressDetail } from "../models/address-detail";

const USERS: User[] = [
  { id: "1", nombre: "Dr Nice", apellido: "ape" },
  { id: "2", nombre: "Narco", apellido: "ape" },
  { id: "3", nombre: "Bombasto", apellido: "ape" }
];

const DEFAULT_ADDRESS = {
  calle: "Calle Falsa",
  altura: "123",
  ciudad: "Azul",
  provincia: "Buenos Aires",
  pais: "Argentina"
};

@Injectable({
  providedIn: "root"
})
export class UserService {
  baseUrl: string = "http://localhost:3000";

  constructor(private httpClient: HttpClient) {}

  async getListPersonaA() {
    const promise = await this.httpClient
      .get(this.baseUrl + "/listPersonaA")
      .toPromise();
    return this.getArrayFromPromise<User>(promise);
  }

  async getListPersonaB() {
    const promise = await this.httpClient
      .get(this.baseUrl + "/listPersonaB")
      .toPromise();
    return this.getArrayFromPromise<User>(promise);
  }

  async getListaDireccionC(personaId: string) {
    const promise = await this.httpClient
      .get(this.baseUrl + "/listaDireccionC?personaId=" + personaId)
      .toPromise();
    return this.getArrayFromPromise<Address>(promise);
  }

  async getListaDireccionD(personaId: string) {
    const promise = await this.httpClient
      .get(this.baseUrl + "/listaDireccionD?personaId=" + personaId)
      .toPromise();
    return this.getArrayFromPromise<Address>(promise);
  }

  async getListaCP() {
    const promise = await this.httpClient
      .get(this.baseUrl + "/listaCP")
      .toPromise();
    return this.getArrayFromPromise<PostalCode>(promise);
  }

  getArrayFromPromise<T>(promise: Object) {
    const json = JSON.stringify(promise);
    let result: Array<T> = JSON.parse(json);
    return result;
  }

  async getUsers() {
    const p = new Promise<User[]>(resolve => {
      setTimeout(() => resolve(USERS), 2000);
    });
    return p.then(data => data);
  }

  async obtenerListadoPersonas(campoOrden: string) {
    const users: UserDetail[] = [];

    let listPersonaA = await this.getListPersonaA();
    let listPersonaB = await this.getListPersonaB();

    var merged = this.mergeArraysWithoutDuplicates<User>(
      listPersonaA,
      listPersonaB,
      "id"
    );

    for (let user of merged) {
      let addressDetail = await this.getAddressDetail(user.id);
      let userDetail = new UserDetail(
        user.id,
        user.nombre,
        user.apellido,
        addressDetail
      );
      users.push(userDetail);
    }
    users.sort((left, right) => {
      let comparison = 0;
      if (left.id < right.id) comparison = -1;
      if (left.id > right.id) comparison = 1;
      return campoOrden == "desc" ? comparison * -1 : comparison;
    });
    return users;
  }

  mergeArraysWithoutDuplicates<T>(
    array1: Array<T>,
    array2: Array<T>,
    key: string
  ) {
    let filterKeys = new Set(array1.map(d => d[key]));
    let filterArray = array2.filter(d => !filterKeys.has(d[key]));
    return [...array1, ...filterArray];
  }

  async getDireccionByID(personaId: string) {
    let address = DEFAULT_ADDRESS;

    const listaDireccionC = await this.getListaDireccionC(personaId);
    const listaDireccionD = await this.getListaDireccionD(personaId);

    if (listaDireccionD && listaDireccionD.length > 0) {
      address = listaDireccionD[0];
    }
    if (listaDireccionC && listaDireccionC.length > 0) {
      address = listaDireccionC[0];
    }
    return address;
  }

  async getPostalCodeByAddress(
    calle: string,
    ciudad: string,
    provincia: string
  ) {
    let postalCode = "";
    const listaCP = await this.getListaCP();
    const result = listaCP.filter(
      x => x.calle === calle && x.ciudad === ciudad && x.provincia === provincia
    );
    if (result && result.length > 0) {
      postalCode = result[0].codigoPostal;
    }
    return postalCode;
  }

  async getAddressDetail(personaId: string) {
    const address = await this.getDireccionByID(personaId);

    let codigoPostal = "";

    if (address) {
      codigoPostal = await this.getPostalCodeByAddress(
        address.calle,
        address.ciudad,
        address.provincia
      );
    }

    let addressDetail = new AddressDetail(
      address.calle + " " + address.altura,
      address.ciudad,
      address.provincia,
      codigoPostal,
      address.pais
    );
    return addressDetail;
  }
}
