import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginInfo } from "../models/login-info";
import { UserLoginInfo } from "../models/user-login-info";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  url: string = "http://localhost:3001/v1/LoginInfo";

  constructor(private httpClient: HttpClient) {}

  async getUserRecentsLogins() {
    const promise = await this.httpClient
      .get(this.url + "/UserRecentsLogins")
      .toPromise();
      return this.getArrayFromPromise<LoginInfo>(promise);
  }

  async getUsersWithMoreLogins() {
    const result: UserLoginInfo[] = [];
    const promise = await this.httpClient
      .get(this.url + "/UsersWithMoreLogins")
      .toPromise();
     const array = this.getArrayFromPromise<LoginInfo>(promise);

     const grouped = this.groupBy<LoginInfo>(array, 'id')
     const keys = Object.keys(grouped);

     keys.forEach(function(key){ 
        const a = grouped[key];
        const nombre = a[0].nombre;
        let loginDate: Date[] = [];
        a.forEach( function(login){
          loginDate.push(login.loginDate);
        })

        loginDate.sort((left, right) => {
          let comparison = 0;
          if (left< right) comparison = -1;
          if (left > right) comparison = 1;
          return comparison * -1;
        });

        result.push({id:key, nombre, loginDate});
    });
    return result;
  }

  getArrayFromPromise<T>(promise:Object) {
    const json = JSON.stringify(promise);
    let result: Array<T> = JSON.parse(json);
    return result;
  }

  groupBy<T>(xs: Array<T>, key:string) {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };
}
