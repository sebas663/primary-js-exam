export class UserLoginInfo {
  id: string;
  nombre: string;
  loginDate: Date[] = [];

  constructor(id: string, nombre: string, loginDate: Date[]) {
    this.id = id;
    this.nombre = nombre;
    this.loginDate = loginDate;
  }
}
