import { AddressDetail } from "../models/address-detail";
export class UserDetail {
  id: string;
  nombre: string;
  apellido: string;
  direccion: AddressDetail;
  
  constructor(
    id: string,
    nombre: string,
    apellido: string,
    direccion: AddressDetail
  ) {
    this.id = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.direccion = direccion;
  }
}
