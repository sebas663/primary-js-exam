export class PostalCode {
  codigoPostal: string;
  calle: string;
  ciudad: string;
  provincia: string;
}
