export class AddressDetail {
  calleAltura: string;
  ciudad: string;
  provincia: string;
  codigoPostal: string;
  pais: string;

  constructor(
    calleAltura: string,
    ciudad: string,
    provincia: string,
    codigoPostal: string,
    pais: string
  ) {
    this.calleAltura = calleAltura;
    this.ciudad = ciudad;
    this.provincia = provincia;
    this.codigoPostal = codigoPostal;
    this.pais = pais;
  }
}
