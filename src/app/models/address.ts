export class Address {
  personaId: string;
  calle: string;
  altura: string;
  ciudad: string;
  provincia: string;
  pais: string;
}
